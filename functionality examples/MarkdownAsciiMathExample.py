meta = {
    'author': '<your name>',
    'title': 'ILIAS-Markdown-und-AsciiMath-Beispiel',
    'type': 'javaScript',
    'instances': 1,
}

task = """ 
Der eingegebene Markdown-Text (mit AsciiMath-Inhalten) wird durch einen Druck auf den neben dem Textfield liegenden Knopf umgewandelt und unter diesem dargestellt!

[editor(rawMarkdown)]markdown[/editor]

[button(mBut)]convert(),Umwandeln[/button]

[text(markdownResultField)]Hier steht dann das Ergebnis der Umwandlung.[/text]
"""

correctAnswers = """
Answer A : 0.0P
"""

appendedData = """

"""

javaScriptCode = """

function convert(){
  var converted = HGMDAndAMToHTML(HGInput("rawMarkdown").HGGetter());
  HGUtility("markdownResultField").innerHTML = converted;
  HGProcessLatex(HGElement("markdownResultField"));
  HGSaveInputs();
}
"""

feedback = """ feedback """