meta = {
    'author': '<your name>',
    'title': 'Python-Beispiel',
    'type': 'javaScript',
    'instances': 1,
}

task = """ 
Der Code in dem folgenden Feld kann per Knopfdruck ausgeführt werden:

[editor(pythonEditor)]python[/editor]

[button(testButton)]testPython(),Ausführen[/button]
[text(pythonOutput)]Hier steht dann die Ausgabe.[/text]
"""

correctAnswers = """
Antwort : 0.0P
"""

appendedData = """

"""

javaScriptCode = """
editor = HGInput("pythonEditor");

function OutputHandler(content){
    HGUtility("pythonOutput").HGSetter(content);
}

function ErrorHandler(error){
    alert(error);
}

function testPython(){
    HGRunPython(editor.HGGetter(), OutputHandler, ErrorHandler);
}
"""

feedback = """ feedback """
